﻿using UnityEngine;
using UnityEngine.U2D;

public class FeliceLogic : MonoBehaviour
{
    const float RunningSpeed = 3.5f;

    public SpriteAtlas sprites;

    private SpriteRenderer spriteRenderer;
    private float timer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites.GetSprite("Felice_Jogging_Frame1_v2");
    }
    void Update()
    {
        bool left = Input.GetKey(KeyCode.A), right = Input.GetKey(KeyCode.D);
        if (left != right)
        {
            // change sprites
            float dt = Time.deltaTime;
            timer += dt;
            int index = (int)((timer % 1) * 12);
            spriteRenderer.sprite = sprites.GetSprite("Felice_Jogging_Frame" + (index + 1) + "_v2");

            // move
            if (left)
            {
                transform.position += new Vector3(-RunningSpeed * dt, 0, 0);
                spriteRenderer.flipX = true;
            }
            else
            {
                transform.position += new Vector3(RunningSpeed * dt, 0, 0);
                spriteRenderer.flipX = false;
            }
        }
    }
}
